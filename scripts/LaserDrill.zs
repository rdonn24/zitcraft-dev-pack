//Used to modify the MFR Laser Drill for balance and cull junk ores.

import mods.mfr.MiningLaser;

MiningLaser.removeOre(<NetherOres:tile.netherores.ore.0>);    #Nether Coal Ore
MiningLaser.removeOre(<NetherOres:tile.netherores.ore.0:3>);  #Nether Iron Ore
MiningLaser.removeOre(<NetherOres:tile.netherores.ore.0:5>);  #Nether Redstone Ore
MiningLaser.removeOre(<NetherOres:tile.netherores.ore.0:6>);  #Nether Copper Ore
MiningLaser.removeOre(<NetherOres:tile.netherores.ore.0:7>);  #Nether Tin Ore
MiningLaser.removeOre(<NetherOres:tile.netherores.ore.0:4>);  #Nether Lapis Ore
MiningLaser.removeOre(<NetherOres:tile.netherores.ore.0:2>);  #Nether Gold Ore
MiningLaser.removeOre(<NetherOres:tile.netherores.ore.0:10>); #Nether Lead Ore
MiningLaser.removeOre(<NetherOres:tile.netherores.ore.1:2>);  #Nether Pig Iron Ore
MiningLaser.removeOre(<NetherOres:tile.netherores.ore.0:1>);  #Nether Diamond Ore
MiningLaser.removeOre(<NetherOres:tile.netherores.ore.1:5>);  #Nether Sulfur Ore
MiningLaser.removeOre(<minecraft:emerald_ore>);
MiningLaser.removeOre(<BiomesOPlenty:gemOre:2>); #Ruby Ore
MiningLaser.removeOre(<BiomesOPlenty:gemOre:12>); #Sapphire Ore
MiningLaser.removeOre(<BiomesOPlenty:gemOre:4>); #Peridot Ore
MiningLaser.removeOre(<BiomesOPlenty:gemOre:6>); #Topaz Ore
MiningLaser.removeOre(<BiomesOPlenty:gemOre:8>); #Tanzanite Ore
MiningLaser.removeOre(<BiomesOPlenty:gemOre:10>); #Malachite Ore
MiningLaser.removeOre(<BiomesOPlenty:gemOre:14>); #Amber Ore
MiningLaser.removeOre(<Metallurgy:fantasy.ore:11>); #Orichalcum Ore
MiningLaser.removeOre(<Metallurgy:fantasy.ore:8>); #Rubracium Ore
MiningLaser.removeOre(<Metallurgy:fantasy.ore:6>); #Carmot Ore
MiningLaser.removeOre(<Metallurgy:fantasy.ore:5>); #Astral Silver Ore
MiningLaser.removeOre(<Metallurgy:fantasy.ore:4>); #Oureclase Ore
MiningLaser.removeOre(<Metallurgy:fantasy.ore:2>); #Infuscolium Ore
MiningLaser.removeOre(<Metallurgy:fantasy.ore:1>); #Deep Iron Ore
MiningLaser.removeOre(<Metallurgy:fantasy.ore>); #Prometheum Ore
MiningLaser.removeOre(<GalacticraftCore:tile.moonBlock:2>); #Cheese Ore
MiningLaser.removeOre(<GalacticraftCore:tile.gcBlockCore:7>);    #GC Aluminum Ore
//MiningLaser.removeOre(<>);    #







//Increase Yellorite Rate
MiningLaser.removeOre(<BigReactors:YelloriteOre>);
MiningLaser.addOre(<BigReactors:YelloriteOre>.weight(30));

//Add Adamantine
MiningLaser.addOre(<Metallurgy:fantasy.ore:13>.weight(23));
MiningLaser.addPreferredOre(6, <Metallurgy:fantasy.ore:13>);