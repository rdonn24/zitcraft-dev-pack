-------------------------------------------------
# zitCraft DEV-CLIENT Custom Mod Pack

This Repo is a **DEV PACK** for the **zitCraft CLIENT** Custom Mod Pack.

# Website

zitCraft Official Website: http://steamtrade.me/projects/zc/zitcraft

-------------------------------------------------
# TODO List:

### Recipes:
    - Other OP items that need nerfed/balanced to delay until "late-game"?
        List:

### Config:
    - Add EnderIO SagMill and AlloySmelter recipes for all ores and alloys.

### Ore/World Generation:
    - 

### Verify:
    -

### HQM:
    - Finish quest lines
    - Fix existing Quests (detection/rewards)
    - Remove empty quest(s)
    - Special quest rewards for zC2.0 Server members?

### General:
    - Generate/find good world seed
    - Setup a "starter town" once a good world seed is found
    - Start prepping official server for the roll-over to 2.0
    - Prep website for 2.0
    - Modpack details in Wiki?
    - Enlist staff member(s) for Moderating and/or Public Relations

-------------------------------------------------
# Setting up the zitCraft 2.0 dev pack client:

### Make sure to follow/read each step VERY carefully!
-------------------------------------------------
## Initial Setup:

- Setup an account here:
https://bitbucket.org/

- Download & Install:
http://www.sourcetreeapp.com/

- Install the Vanilla Minecraft Launcher:
https://minecraft.net/download

- Download (do NOT install) Forge:
http://adf.ly/673885/http://files.minecraftforge.net/maven/net/minecraftforge/forge/1.7.10-10.13.2.1291/forge-1.7.10-10.13.2.1291-installer-win.exe

- Make sure you have the latest version of Java *7* installed (NOT JAVA 8):
http://www.oracle.com/technetwork/java/javase/downloads/jre7-downloads-1880261.html

-------------------------------------------------
## SourceTree Setup:
- Open your web browser to BitBuket's website, and login
- Go here: https://bitbucket.org/rdonn24/zitcraft-dev-pack
- In the upper-right-hand corner (next to where it says HTTPS) copy the URL listed in the box
- Open SourceTree
- Click on "Clone"
- Enter the URL from your clipboard
- Pick the folder you made earlier, for the dev client
- Clone from the "dev" branch (not master)
- Wait for it to download (approx. 1.4 GB)
- In SourceTree, in the left-hand side-bar, double click on the "dev" branch (so it's *bold* [active])

-------------------------------------------------
## Launcher Setup:
- Open Windows Explorer, and create a new folder, where you want to save the dev client

- Open the MC Launcher, and launch the 1.7.10 Vanilla version of MC
- Close MC, and the Vanilla MC Launcher

- Run the Forge Installer for Windows
- Install Forge to the default location

- Create a new profile in the MC Vanilla Launcher:
- Point the newly created profile to the dev client folder you just created in the last step
- Under "Use Version", pick the proper version of Forge (1291)
- Check the box, to activte the JVM arguments
- Set the argument -Xmx1G to -Xmx2G (minimum), I recommend setting it to: -Xmx4G

-------------------------------------------------
## Launching the dev client:
- Open the Vanilla MC Launcher
- Select the Forge Profile you created
- Launch the game!